package pl.sda.quiz;

import java.util.List;

public class Question {
    private String question;
    private List<String> answers;
    private String rightAnsewer;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
        this.rightAnsewer = answers.get(0);
    }

    public boolean isAnswerCorrect(String answer) {
        return answer.equals(rightAnsewer);
    }


    @Override
    public String toString() {
        return '{' + question + '\'' +
                ", answers=" + answers +
                '}';
    }


    @Override
    public int hashCode() {
        return this.getQuestion().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof Question) {
            Question otherQuestion = (Question) obj;
            return this.getQuestion().equals(otherQuestion.getQuestion());
        }
        return false;
    }

}
