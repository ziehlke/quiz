import pl.sda.quiz.Question;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    private final static Scanner input = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        Map<String, List<Question>> questionMap = new TreeMap<>();
        File directory = new File("src/main/resources/quiz");

        for (File file : directory.listFiles()) {
            String cathegory = file.getName().substring(0, file.getName().length() - 4);
            List<Question> questions = readAllQuestionsFromFile(file);
            questionMap.put(cathegory, questions);
        }

        System.out.println("Wybierz kategorie: ");
        String choosenCathegory = chooseCathegory(questionMap);

        Set<Question> drawnQuestions = drawQuestions(choosenCathegory, questionMap);
        playQuiz(drawnQuestions);


    }

    private static void playQuiz(Set<Question> drawnQuestions) {
        for (Question question : drawnQuestions) {
            List<String> answers = question.getAnswers();

            System.out.println(question.getQuestion());


            Collections.shuffle(answers);
            for (int j = 0; j < answers.size(); j++) {
                System.out.println(j + 1 + ". " + answers.get(j));
            }

            int theAnswer = input.nextInt();
            if (question.isAnswerCorrect(answers.get(theAnswer - 1))) {
                System.out.println("dobrze");
            }


        }
    }

    private static Set<Question> drawQuestions(String choosenCathegory, Map<String, List<Question>> questionMap) {
        Set<Question> drawn = new HashSet<>();
        Random random = new Random();


        while (drawn.size() < 10) {
            int randomNumber;
            Question drawnQuestion;
            if (!choosenCathegory.equals("X")) {
                randomNumber = random.nextInt(questionMap.get(choosenCathegory).size());
                drawnQuestion = questionMap.get(choosenCathegory).get(randomNumber);

            } else {
                List<Question> allQuestions = questionMap.values().stream()
                        .flatMap(Collection::stream)
                        .collect(Collectors.toList());

                randomNumber = random.nextInt(allQuestions.size());
                drawnQuestion = allQuestions.get(randomNumber);

            }
            drawn.add(drawnQuestion);
        }


        return drawn;
    }

    private static String chooseCathegory(Map<String, List<Question>> questionMap) {
        for (int i = 0; i < questionMap.keySet().size(); i++) {
            System.out.println(String.valueOf(i + 1) + ". " + questionMap.keySet().toArray()[i]);
        }
        System.out.println("0. Pytania ze wszystkich kategorii.");


        int choice;
        try {
            choice = input.nextInt();
            if (choice < 0 || choice > questionMap.keySet().size()) throw new InputMismatchException();
            System.out.println("Losuję 10 pytań z: " + questionMap.keySet().toArray()[choice - 1]);

        } catch (InputMismatchException e) {
            System.out.println("Nie rozpoznałem kategorii - losuję spośród wszystkich.");
            return "X";
        }
        return (String) questionMap.keySet().toArray()[choice - 1];
    }


    private static List<Question> readAllQuestionsFromFile(File file) {
        try (Scanner scanner = new Scanner(file)) {

            List<Question> questions = new ArrayList<>();
            while (scanner.hasNextLine()) {
                Question question = new Question();
                question.setQuestion(scanner.nextLine());
                List<String> answers = new ArrayList<>();
                int numberOfAnswers = Integer.parseInt(scanner.nextLine());
                for (int i = 0; i < numberOfAnswers; i++) {
                    answers.add(scanner.nextLine());
                }
                question.setAnswers(answers);
                questions.add(question);

            }
            return questions;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
